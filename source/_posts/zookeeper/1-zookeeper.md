---
title: ZooKeeper
date: 2020-03-04
tags: [ZooKeeper]
categories: 
  - ZooKeeper
---

转载请标注原文地址：https://lilyssh.cn/zookeeper/2-zookeeper/

<!-- more -->
# 一、ZooKeeper是什么？
ZooKeeper是个开源的分布式系统的协调组件，用来协作多个任务。
# 二、ZooKeeper能用来做什么？
配置管理、名字服务、分布式锁、集群管理。
# 三、为什么要使用ZooKeeper？
使开发人员更多关注应用本身逻辑，而不是协同工作上，保证项目健壮性。
而且具有高容错性和可扩展性。

集群版
Zookeeper 集群中的角色
设计目的

# 群首选举
