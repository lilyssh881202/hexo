---
title: spring中用到的设计模式
date: 2019-12-12
tags: [设计模式]
categories: 
  - 设计模式
---

<!-- more -->

Spring IOC 工厂、单例、装饰器 
Spring AOP 代理、观察者 
Spring MVC 委派、适配器 
Spring JDBC 模板方法

## 1)、工厂模式
BeanFactory
## 2)、装饰器模式
BeanWrapper
## 3)、代理模式
AopProxy
## 4)、单例模式
ApplicationContext
## 5)、委派模式
DispatcherServlet
## 6)、策略模式
HandlerMapping
## 7)、适配器模式
HandlerAdapter
## 8)、摸板方法模式
JdbcTemplate
## 9)、观察者模式
ContextLoaderListener