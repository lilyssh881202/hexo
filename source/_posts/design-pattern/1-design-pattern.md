---
title: 设计模式
date: 2019-12-12
tags: [设计模式]
categories: 
  - 设计模式
---
# 1、软件设计的原则
<!-- more -->
SOLID
|     |                               |             |
| :--:| :---------------------------: | :---------: |
| SRP	| Simple Responsibility Pinciple | 单一责任原则 |
| OCP	| Open-Closed Principle | 开放封闭原则 |
| LSP	| Liskov Substitution Principle | 里氏替换原则 |
| ISP	| Interface Segregation Principle | 接口隔离原则 |
| DIP	| Dependence Inversion Principle | 依赖倒置原则 |
|     |                               |             |

## 1)、单一职责原则
一个Class/Interface/Method只负责一项职责。
## 2)、开闭原则
对扩展开放，对修改关闭。核心思想是面向抽象编程。
## 3)、里氏替换原则
子类可以扩展父类的功能，但不能改变父类原有的功能。
### (1).子类可以实现父类的抽象方法，但不能覆盖父类的非抽象方法。 
### (2).子类中可以增加自己特有的方法。 
### (3).当子类的方法重载父类的方法时，方法的前置条件（即方法的输入/入参）要比父类方法的输入参数更宽松。 
### (4).当子类的方法实现父类的方法时（重写/重载或实现抽象方法），方法的后置条件（即方法的输出/返回值）要比父类更严格或相等。
## 4)、接口隔离原则
类不应该依赖不需要的接口，知道越少越好。
是指用多个专门的接口，而不使 用单一的总接口。
## 5)、依赖倒置原则
### (1).高层模块不应该依赖低层模块，两者都应该依赖其抽象。
### (2).抽象不应该依赖细节。
### (3).细节应该依赖抽象。
比如类A内有类B对象，称为类A依赖类B，但是不应该这样做，而是选择类A去依赖抽象。

## 6)、迪米特法则
一个对象应该对其他对象保持最少的了解，又 叫最少知道原则(Least Knowledge Principle,LKP)，尽量降低类与类之间的耦合。
## 7)、合成复用原则
尽量使用对象组合(has-a)/聚合(contanis-a)，而不是继承关系达到软件复用的目的。
