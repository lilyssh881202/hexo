---
title: 自定义RPC框架实战(二) 实现
date: 2018-02-02
tags: [RPC]
categories: 
  - RPC
---

转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/84306533
项目源码地址：https://gitee.com/lilyssh/lilyssh-rpc
# 一、创建项目：
我们需要写三个项目：注册中心registry、rpc框架lrpc 和 使用lrpc的demo项目。demo和registry都是`Spring Boot`项目，lrpc是一个`jar`包，demo引用lrpc的`jar`包，使lrpc嵌入到demo里，这样启动demo时lrpc就会把服务信息捞出来，封装成lrpc和registry约定好的`json`格式字符串，通过`netty`发送给registry。  

`pom.xml`中引入插件时，如果是`Spring Boot`项目，就用`Spring Boot`的`Maven`插件（`spring-boot-maven-plugin`），它能够将`Spring Boot`应用打包为可执行的`jar`或`war`文件，然后以通常的方式运行`Spring Boot`应用。
```xml
<build>
  <plugins>
    <plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>
    </plugin>
  </plugins>
</build>
```
非`spring boot`项目，就用原装`maven`插件：`maven-compiler-plugin`，`maven`是个项目管理工具，如果我们不告诉它我们的代码要使用什么样的`jdk`版本编译的话，它就会用`maven-compiler-plugin`默认的`jdk`版本来进行处理，这样就容易出现版本不匹配，以至于可能导致编译不通过的问题。`maven`的默认编译使用的jdk版本貌似很低，使用`maven-compiler-plugin`插件可以指定项目源码的jdk版本，编译后的`jdk`版本，以及编码。
```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <configuration>
                <source>1.8</source>
                <target>1.8</target>
            </configuration>
        </plugin>
    </plugins>
</build>
```
# 二、注册服务

- 暴露本地服务
- 暴露远程服务
- 启动netty
- 连接zookeeper
- 到zookeeper注册
- 监听zookeeper

为什么会有本地暴露和远程暴露呢？在dubbo中一个服务可能既是Provider，又是Consumer，因此就存在他自己调用自己服务的情况，如果再通过网络去访问，那自然是舍近求远，因此要有本地暴露服务。两者的区别：
- 本地暴露是暴露在JVM中,不需要网络通信。
- 远程暴露是将ip,端口等信息暴露给远程客户端,调用时需要网络通信。

## 1.在lrcp中通过`netty`发送信息给registry，registry接受字符串。
为什么选择`netty`呢，因为它并发高，传输快，封装好。`Netty`是一款基于`NIO`（`Nonblocking I/O`，非阻塞IO）开发的网络通信框架，对比于`BIO`（`Blocking I/O`，阻塞IO），他的并发性能得到了很大提高。
具体实现请参考： https://lilyssh.cn/rpc/3-netty/
## 2.在lrcp中写个自定义注解，包装服务，如@LrpcService。扫描到这些的时候，就自动捞取了。
具体实现请参考： https://lilyssh.cn/rpc/4-custom-annotation/
## 3. 使用socket进行通讯，与netty对比效率。
spring boot 在启动时，会对每个类进行扫描，每扫描到有自定义注解@LrpcService时，就由lrcp主动推送给registry。