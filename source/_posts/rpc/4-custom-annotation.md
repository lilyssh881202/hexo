---
title: 如何获取自定义注解使用者信息 
date: 2018-02-04
tags: [Annotation]
categories: 
  - Annotation
---

转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/84306592
项目源码地址：https://gitee.com/lilyssh/lilyssh-rpc

# 一、项目需求
在启动项目时，获取自定义注解使用者的信息。
# 二、项目结构
工程 lilyssh-rpc 的结构为：
```
$ tree -I target
.
├── lrpc
│   ├── pom.xml
│   └── src
│       └── main
│           ├── java
│           │   └── cn
│           │       └── lilyssh
│           │           └── rpc
│           │               ├── annotation
│           │               │   └── LrpcService.java
│           │               └── processor
│           │                   └── AnnotationInitializeProcessor.java
│           └── resources
│               ├── META-INF
│               │   └── spring.factories
│               └── application.yml
├── pom.xml
└── demo
    ├── pom.xml
    └── src
        └── main
            ├── java
            │   └── cn
            │       └── lilyssh
            │           └── demo
            │               ├── DemoApplication.java
            │               └── service
            │                   └── UserService.java
            └── resources
                └── application.yml
```
lrpc是创建自定义注解的项目。demo是使用自定义注解的项目。
# 三、lrpc项目：
## 1. 引入依赖
```xml
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-beans</artifactId>
    <version>5.1.2.RELEASE</version>
    <scope>compile</scope>
</dependency>
```
## 2. 自定义注解
```java
package cn.lilyssh.rpc.annotation;
import org.springframework.stereotype.Component;
import java.lang.annotation.*;

@Component
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface LrpcService {
}
```
## 3. 监听每个bean的注入
```java
package cn.lilyssh.rpc.processor;

import cn.lilyssh.rpc.annotation.LrpcService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;

public class AnnotationInitializeProcessor  implements BeanPostProcessor {

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        //获取bean的class对象
        Class<? extends Object> clazz = bean.getClass();
        //判断该bean是否用了LrpcService注解
        LrpcService lrpcService = AnnotationUtils.findAnnotation(clazz, LrpcService.class);
        if(null!=lrpcService){
            System.out.println("------------------------"+bean.toString());
            System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^"+lrpcService.toString());
        }else{
            System.out.println("++++++++++++++++++++++++"+bean.toString());
        }
        return bean;
    }
}
```
## 4. 扫描bean注入监听类
在`resources/META-INF`下创建`spring.factories`，内容如下：
```
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
  cn.lilyssh.rpc.processor.AnnotationInitializeProcessor
```

# 四、demo项目：
使用自定义注解的spring boot项目。
## 1. 引入依赖：
```xml
<dependency>
  <groupId>cn.lilyssh</groupId>
  <artifactId>lrpc</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```
## 2. 使用自定义注解：
```java
package cn.lilyssh.demo.service;
import cn.lilyssh.rpc.annotation.LrpcService;

@LrpcService
public class UserService {
}
```
# 五、测试：
启动demo项目，会看到：
![](https://resource.lilyssh.cn/pic/custom-annotation.png)
大功告成！