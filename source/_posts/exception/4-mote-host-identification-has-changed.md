---
title: 'ssh警告: REMOTE HOST IDENTIFICATION HAS CHANGED!'
date: 2018-01-01
tags: [Exception]
categories: 
  - Exception
---
  
# 1. 异常描述
<!-- more -->
在远程连接服务器时报错：
```
~ ⌚ 14:03:35
$ ssh root@ssh.qianxunclub.com
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:XoSjIjyhyQYeWVkAwwRdB/akalMTPJIRBoShLfMxEVY.
Please contact your system administrator.
Add correct host key in /Users/cypher02/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /Users/cypher02/.ssh/known_hosts:1
ECDSA host key for ssh.qianxunclub.com has changed and you have requested strict checking.
Host key verification failed.
```
# 2. 异常分析
ssh会把每个你访问过计算机的公钥(public key)都记录在~/.ssh/known_hosts。当下次访问相同计算机时，OpenSSH会核对公钥。如果公钥不同，OpenSSH会发出警告， 避免你受到DNS Hijack之类的攻击。
此处原因是由于把服务器的系统从centos换成了ubantu.
# 3. 解决办法
有以下两个解决方案：
1. 删除known_hsots； 
```
rm -rf .ssh/known_hosts
```
2. 修改配置文件“~/.ssh/config”，加上这两行，重启服务器。 
```
StrictHostKeyChecking no 
UserKnownHostsFile /dev/null 
```

大功告成！