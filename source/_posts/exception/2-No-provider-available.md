---
title: dubbo服务调用异常：No provider available for the service
date: 2018-01-06
tags: [Exception,Dubbo,Spring Boot]
categories: 
  - Exception
---
 转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/82775678  
 
[项目源码地址](https://gitee.com/lilyssh/high-concurrency)  
# 1. 异常描述
在启动基于`spring boot`的order-consumer项目时，报错：
<!-- more -->
```java
Error starting ApplicationContext. To display the conditions report re-run your application with 'debug' enabled.
2018-09-19 16:14:34.167 ERROR 2615 --- [           main] o.s.boot.SpringApplication               : Application run failed

org.springframework.beans.factory.UnsatisfiedDependencyException: Error creating bean with name 'orderController' defined in file [/Users/cypher02/workspace/gitee/high-concurrency/order/order-consumer/target/classes/cn/lilyssh/order/consumer/controller/OrderController.class]: Unsatisfied dependency expressed through constructor parameter 0; nested exception is org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'orderService' defined in file [/Users/cypher02/workspace/gitee/high-concurrency/order/order-consumer/target/classes/cn/lilyssh/order/consumer/service/OrderService.class]: Initialization of bean failed; nested exception is org.springframework.beans.factory.BeanCreationException: orderService; nested exception is java.lang.IllegalStateException: Failed to check the status of the service cn.lilyssh.goods.api.service.GoodsServiceApi. No provider available for the service cn.lilyssh.goods.api.service.GoodsServiceApi from the url zookeeper://ssh.qianxunclub.com:2181/com.alibaba.dubbo.registry.RegistryService?application=order-consumer&dubbo=2.6.0&interface=cn.lilyssh.goods.api.service.GoodsServiceApi&methods=exist,saveBatch,save,list,updateById&pid=2615&register.ip=192.168.31.31&side=consumer&timestamp=1537344873982 to the consumer 192.168.31.31 use dubbo version 2.6.0
	at org.springframework.beans.factory.support.ConstructorResolver.createArgumentArray(ConstructorResolver.java:732) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.beans.factory.support.ConstructorResolver.autowireConstructor(ConstructorResolver.java:197) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.autowireConstructor(AbstractAutowireCapableBeanFactory.java:1267) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBeanInstance(AbstractAutowireCapableBeanFactory.java:1124) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.doCreateBean(AbstractAutowireCapableBeanFactory.java:535) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.beans.factory.support.AbstractAutowireCapableBeanFactory.createBean(AbstractAutowireCapableBeanFactory.java:495) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.beans.factory.support.AbstractBeanFactory.lambda$doGetBean$0(AbstractBeanFactory.java:317) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.beans.factory.support.DefaultSingletonBeanRegistry.getSingleton(DefaultSingletonBeanRegistry.java:222) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(AbstractBeanFactory.java:315) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.beans.factory.support.AbstractBeanFactory.getBean(AbstractBeanFactory.java:199) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.beans.factory.support.DefaultListableBeanFactory.preInstantiateSingletons(DefaultListableBeanFactory.java:759) ~[spring-beans-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.context.support.AbstractApplicationContext.finishBeanFactoryInitialization(AbstractApplicationContext.java:869) ~[spring-context-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:550) ~[spring-context-5.0.8.RELEASE.jar:5.0.8.RELEASE]
	at org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext.refresh(ServletWebServerApplicationContext.java:140) ~[spring-boot-2.0.4.RELEASE.jar:2.0.4.RELEASE]
	at org.springframework.boot.SpringApplication.refresh(SpringApplication.java:762) [spring-boot-2.0.4.RELEASE.jar:2.0.4.RELEASE]
	at org.springframework.boot.SpringApplication.refreshContext(SpringApplication.java:398) [spring-boot-2.0.4.RELEASE.jar:2.0.4.RELEASE]
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:330) [spring-boot-2.0.4.RELEASE.jar:2.0.4.RELEASE]
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:1258) [spring-boot-2.0.4.RELEASE.jar:2.0.4.RELEASE]
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:1246) [spring-boot-2.0.4.RELEASE.jar:2.0.4.RELEASE]
	at cn.lilyssh.order.consumer.OrderConsumerApplication.main(OrderConsumerApplication.java:12) [classes/:na]
Disconnected from the target VM, address: '127.0.0.1:56417', transport: 'socket'
Process finished with exit code 1
```
# 2. 异常分析
No provider available for the service cn.lilyssh.goods.api.service.GoodsServiceApi
从异常的信息看，有两种可能：
1. 在zookeeper里没注册进去服务
2. 注册进去了，但是拿不出来。
进去zookeeper里看看就知道怎么肥四了：
```
dubbo://192.168.31.31:3333/cn.lilyssh.order.api.service.OrderServiceApi?anyhost=true&application=order-provider&default.timeout=5000&dubbo=2.6.0&generic=false&interface=cn.lilyssh.order.api.service.OrderServiceApi&methods=saveBatch,save,list&pid=2541&revision=1.2.3&side=provider&timestamp=1537344229809&version=1.2.3
```
最后是 version=1.2.3
原来是往外抛服务时，加了版本号：
```java
@Slf4j
@Service(version = "1.2.3")
@Component
@AllArgsConstructor
public class GoodsService implements GoodsServiceApi {

}
```
而在order-coonsumer项目中，引用时，却没加版本号
```java
@Slf4j
@Component
public class OrderService {

    @Reference
    private GoodsServiceApi goodsServiceApi;

    @Reference(version = "1.2.3")
    private OrderServiceApi orderServiceApi;
}
```
# 3. 解决办法

注册和调用的服务的版本号要一致！

大功告成！