---
title: mysql 8.0 主从复制 基本配置
date: 2018-06-19
tags: [MySQL]
categories: 
  - MySQL
---

转载请标注原文地址：https://lilyssh.cn/mysql/1-mysql-replication/
# 简介 
MySQL主从复制可实现数据的多处自动备份。不仅可以加强数据的安全性，通过实现读写分离还能提升数据库的负载性能。  
<!-- more -->
# 实现原理：
![](http://p8wysinsm.bkt.clouddn.com/mysql_replication.jpeg) MySQL之间数据复制的基础是二进制日志文件（binary log file）。一台MySQL数据库一旦启用二进制日志后，其作为master，它的数据库中所有操作都会以“事件”的方式记录在二进制日志中，其他数据库作为slave通过一个I/O线程与主服务器保持通信，并监控master的二进制日志文件的变化，如果发现master二进制日志文件发生变化，则会把变化复制到自己的中继日志中，然后slave的一个SQL线程会把相关的“事件”执行到自己的数据库中，以此实现从数据库和主数据库的一致性，也就实现了主从复制。

# 步骤  
## 主服务器：
- 开启二进制日志
- 配置唯一的server-id
- 获得master二进制日志文件名及位置
- 创建一个用于slave和master通信的用户账号
## 从服务器：
- 配置唯一的server-id
- 使用master分配的用户账号读取master二进制日志
- 启用slave服务

具体实现过程如下：  
一、主数据库master修改：  
1.修改mysql配置

找到主数据库的配置文件my.cnf(或者my.ini)，我的在/etc/mysql/my.cnf,在[mysqld]部分插入如下两行：
```
[mysqld]
log_bin=mysql-bin #开启二进制日志,mysql-bin是二进制日志文件的名称，可自定义命名。
server-id=1 #设置server-id
```
2.重启mysql，创建用于同步的用户账号

打开mysql会话shell>mysql -hlocalhost -uname -ppassword

创建用户并授权：用户：repl 密码：slavepass
```
mysql> CREATE USER 'repl'@'123.57.44.85' IDENTIFIED BY 'slavepass';#创建用户
mysql> GRANT REPLICATION SLAVE ON *.* TO 'repl'@'123.57.44.85';#分配权限
mysql>flush privileges;   #刷新权限
```
3.查看master状态，记录二进制文件名(mysql-bin.000003)和位置(73)：

```
mysql > SHOW MASTER STATUS;
```

|File| Position| Binlog_Do_DB| Binlog_Ignore_DB| Executed_Gtid_Set|
| --- | --- | --- | --- | --- |
| mysql-bin.000004 | 155 |  |  |  |

二、从服务器slave修改：
1.修改mysql配置
同样找到my.cnf配置文件，添加server-id
```
[mysqld]
server-id=2 #设置server-id，必须唯一
```
2.重启mysql，打开mysql会话，执行同步SQL语句(需要主服务器主机名，登陆凭据，二进制文件的名称和位置)：
```
mysql> CHANGE MASTER TO
    ->     MASTER_HOST='192.168.0.110',
    ->     MASTER_USER='repl',
    ->     MASTER_PASSWORD='slavepass',
    ->     MASTER_LOG_FILE='mysql-bin.000003',
    ->     MASTER_LOG_POS=73;
```
3.启动slave同步进程：
```
mysql>start slave;
```
4.查看slave状态：
```
mysql> show slave status\G;
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 182.92.172.80
                  Master_User: rep1
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000013
          Read_Master_Log_Pos: 11662
               Relay_Log_File: mysqld-relay-bin.000022
                Relay_Log_Pos: 11765
        Relay_Master_Log_File: mysql-bin.000013
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
        ...
```
当Slave_IO_Running和Slave_SQL_Running都为YES的时候就表示主从同步设置成功了。