---
title: 网上养狗证办理步骤
date: 2018-01-01
tags: [养狗证]
categories: 
  - 养狗证
---

转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/84143423
由于图片放在国外的github上，所以图片加载会有点慢，如若引起不适，忍着！
# 1. 资料准备：
- 本人有效身份证件  
- 住址证明  
- 犬只照片（全身彩照，四肢站立、侧身回头、能看见犬只双眼。一寸犬头照一张、三寸犬全身照一张）。  
- 狂犬免疫证
![](https://resource.lilyssh.cn/pic/application-material.png)
# 2. 登录广东政务服务网
http://www.gdzwfw.gov.cn/ 注册、登录。不要用QQ浏览器，因为后面会显示不出来单选按钮，LZ换了谷歌浏览器。
# 3. 搜索养犬初始登记
![](https://resource.lilyssh.cn/pic/search-dog-registration.png)
![](https://resource.lilyssh.cn/pic/search-result.png)
选择地点，点击在线办理。
![](https://resource.lilyssh.cn/pic/online-declaration.png)
点击在线申报，会进入养犬初始登记（养犬证）办理页面。
# 4. 填写养犬初始登记（养犬证）办理表
![](https://resource.lilyssh.cn/pic/application.png)
填写完信息以后，点击打印按钮，
![](https://resource.lilyssh.cn/pic/print-application.png)
会下载下来一个pdf文件，由于这里没打印机，所以LZ决定用ps，把狗子照片粘贴到办理表上。如果狗友们有打印机的话，可以打印出来，粘上狗子照片，拍照，转成pdf格式，直接跳过第4步，往下走。
# 4、把照片P到申请表上（如果不想打印出来贴照片的）
## (1) 下载creative-cloud
https://creative.adobe.com/zh-cn/products/download/creative-cloud
## (2) 下载ps
![](https://resource.lilyssh.cn/pic/download-ps.png)
## (3) 把狗子照片粘贴到pdf上
点击 文件 -> 打开，打开pdf、狗子照片，
![](https://resource.lilyssh.cn/pic/open-application.png)
会分别在两个标签页，把狗子照片的标签页缩小一点，然后把狗子照片拖进pdf：
![](https://resource.lilyssh.cn/pic/drag.png)
按住 Ctrl + T ,来调整图片大小，并把照片放在合适的位置，放好后，按Enter键确认。之后，效果如下：
![](https://resource.lilyssh.cn/pic/application-result.png)
## (4) 保存为pdf
点击 文件 -> 存储为，格式选择 Photoshop PDF：
![](https://resource.lilyssh.cn/pic/save-pdf-1.png)
![](https://resource.lilyssh.cn/pic/save-pdf-2.png)
贴好狗子照片版本的 养狗证申请表副本.pdf 就制作完成了！艾玛！不容易呀！
# 5. 上传资料
上传前，需要先把每个照片，转换成pdf格式：
![](https://resource.lilyssh.cn/pic/cast-format.png)
接下来，我们再次回到填办理表的地方，滑到下面，上传资料。
![](https://resource.lilyssh.cn/pic/wait-upload.png)
点击上图红框里的图标，会弹出上传页面：
![](https://resource.lilyssh.cn/pic/upload-1.png)
![](https://resource.lilyssh.cn/pic/upload-2.png)
完全上传完后，后面会显示每个资料的上传张数：
![](https://resource.lilyssh.cn/pic/after-upload.png)
点击下一步，输入邮寄地址：
![](https://resource.lilyssh.cn/pic/at-last.png)
点击下一步，输入手机验证码：
![](https://resource.lilyssh.cn/pic/send-code.png)
点击下一步，截止到文章发布时，还在转圈，LZ F12了一下，艾玛！不忍直视呀：
![](https://resource.lilyssh.cn/pic/last-last.png)
zf网站做得好差劲。。建议zf多花点钱，招点做双十一的程序员过去重新做下。一打出来zf俩字，竟然变成待审核了。。
又重新操作了一遍，成功！！！
![](https://resource.lilyssh.cn/pic/success-1.png)
![](https://resource.lilyssh.cn/pic/success-2.png)
三天后，会收到短信：
![](https://resource.lilyssh.cn/pic/zwfw-msg-1.JPG)
接下来我们再来看几个反面案例，我只想对她们说：“作为失败的典型，你实在是太成功啦！”。
![](https://resource.lilyssh.cn/pic/failed-1.JPG)
![](https://resource.lilyssh.cn/pic/failed-2.JPG)
大功告成！