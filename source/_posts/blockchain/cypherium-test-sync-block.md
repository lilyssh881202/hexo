---
title: Cypherium 同步区块测试操作步骤
date: 2018-08-08
tags: [Blockchain,区块链,Cypherium,以太坊]
categories: 
  - Blockchain
---

转载请标注原文地址：https://lilyssh.cn/blockchain/cypherium-test-sync-block/
# 前言  
Cypherium 同步区块链测试步骤。以下步骤适用于同一台机子测试。如果使用两台电脑测试，则有关第二节点的操作在另一台机子上执行即可。以下命令请在`cypherium_private/go-cypherium`路径下执行。
<!-- more -->
1. 创建test文件夹,test中目录结构如下：

```
~/workspace/test ⌚ 14:02:17
$ tree
.
├── genesis.json
├── group.toml
├── node_01
│   ├── data
└── node_02
    ├── data
```
2. 清理  

```
make clean
```
3. 编译  

```
make cypher
```
4. 生成节点信息  

```
./build/bin/cypher setup
```
需要输入 ip：端口，用于测试使用kyber的Ed25519算法生成`public.toml`和`private.toml`，保存在默认路径。目前没做ip发现，所以需手动拼接公钥`~/Library/Application\ Support/cypher/public.toml`(Mac默认路径)，用于模拟测试。节点名字可自定义，如node_01_name。  
![](https://resource.lilyssh.cn/pic/cypher%20setup_.png)
5. 同理，再生成第二个节点。

6. 把第一、二个节点的public.toml，拼接到~/workspace/test/group.toml中。

![](https://resource.lilyssh.cn/pic/group.toml.png)
7. 初始化 两个节点的创世区块。  

第一个节点：  
```
./build/bin/cypher --datadir "~/workspace/test/node_01/data" init ~/workspace/test/genesis.json
```
第二个节点:  
```
./build/bin/cypher --datadir "~/workspace/test/node_02/data" init ~/workspace/test/genesis.json
```
genesis.json中内容可参考：
```
{
  "config": {
        "chainId": 123666,
        "homesteadBlock": 0,
        "eip155Block": 0,
        "eip158Block": 0
    },
  "alloc"      : {},
  "coinbase"   : "0x0000000000000000000000000000000000000000",
  "difficulty" : "0x20000",
  "extraData"  : "",
  "gasLimit"   : "0x2fefd8",
  "nonce"      : "0x0000000000000042",
  "mixhash"    : "0x0000000000000000000000000000000000000000000000000000000000000000",
  "parentHash" : "0x0000000000000000000000000000000000000000000000000000000000000000",
  "timestamp"  : "0x00"
}
```
8. 启动第一个节点。

```
./build/bin/cypher --onetdir ~/workspace/test/node_01/private.toml --datadir ~/workspace/test/node_01/data --publickeydir ~/workspace/test/group.toml --networkid 123666 --port 7010 --rpcport 8510 --rpc --rpccorsdomain "*" --rpcaddr 0.0.0.0 --rpcapi eth,web3,personal --verbosity 5 2> ~/workspace/test/node_01/output2.log
```
9. 启动第一个节点的js交互窗口(用上一步成的ipc文件)：

```
./build/bin/cypher attach ~/workspace/test/node_01/data/cypher.ipc
```
10. 启动第二个节点。

```
./build/bin/cypher --onetdir ~/workspace/test/node_02/private.toml --datadir ~/workspace/test/node_02/data --publickeydir ~/workspace/test/group.toml --networkid 123666 --port 7020 --rpcport 8520 --rpc --rpccorsdomain "*" --rpcaddr 0.0.0.0 --rpcapi eth,web3,personal --verbosity 5 2> ~/workspace/test/node_02/output2.log
```
11. 启动第二个节点的js交互窗口(用上一步成的ipc文件)。

```
./build/bin/cypher attach ~/workspace/test/node_02/data/cypher.ipc
```
![](https://resource.lilyssh.cn/pic/attach%20node_.jpg)
12. 从此步骤开始，都将在js交互涌窗口执行。查看第一个节点的信息。

```
 admin.nodeInfo
```
13. 在第二个节点中，创建用户，保存挖矿奖励。

```
 personal.newAccount("password")
```
14. 在第二个节点中，加入第一个节点。就是把cnode中的内容粘进addPeer中。

```
 admin.addPeer("cnode")
```
15. 在第一个节点中，验证一下是否已加入。

```
 net
```
16. 主节点调用开始同步命令，从交易池里取tx执行，并打包生成txBlock，发给其他委员会成员做bftcosi共识。会看到两个节点的信息都在滚动。

```
bftcosi.start()
```
17. 停止共识。

```
bftcosi.stop()
```
18. 查看下两个节点的区块数量是否相同。

![](https://resource.lilyssh.cn/pic/txBlockNumber.png)

19. (不时需要)解锁账户

```
personal.unlockAccount("需解锁账户")
```
![](https://resource.lilyssh.cn/pic/unlockAccount.png)
20. 查看交易池状态，pending为待确认的交易数量。
```
 txpool.status
```
会显示
```
{
  pending: 0,
  queued: 0
}
```


19. 退出：

![](https://resource.lilyssh.cn/pic/killall.png)
