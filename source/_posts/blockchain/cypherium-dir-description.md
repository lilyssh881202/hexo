---
title: 以太坊 目录结构说明
date: 2018-08-10
tags: [Blockchain,区块链,以太坊]
categories: 
  - Blockchain
---

转载请标注原文地址：https://lilyssh.cn/blockchain/cypherium-dir-description/  

# 前言  
go-cypherium
本篇为转发，只做了下样式整理，[原文地址](https://blog.csdn.net/jiang_xinxing/article/details/80249981)。
<!-- more -->
```
~/workspace/test ⌚ 14:02:17
$ tree
.
├── accounts      // 实现了高层级的Ethereum账号管理
│   ├── abi       // 实现了Ethereum的ABI(应用程序二进制接口) 
│   │   └── bind  // 该包生成Ethereum合约的Go绑定 
│   │       └── backends
│   ├── keystore  // 实现了Secp256k1私钥的加密存储
│   └── usbwallet // 实现了支持USB硬件钱包
├── cmd
│   ├── abigen
│   ├── bootnode  // 该节点为Ethereum发现协议运行一个引导节点 
│   ├── clef
│   ├── cypher    // 是Cypherium的官方客户端命令行 
│   ├── ethkey
│   ├── evm       // 执行EVM代码片段 
│   ├── faucet    // 是以太faucet支持的轻量级客户 
│   ├── p2psim    // 为客户端命令行模拟 HTTP API 
│   ├── puppeth   // 是一个命令组装和维护私人网路
│   ├── rlpdump   // 能更好的打印出RLP格式的数据
│   ├── swarm     // bzzhash命令能够更好的计算出swarm哈希树 
│   ├── utils     // 为Go-Ethereum命令提供说明
│   └── wnode
├── common        // 包含一些帮助函数
│   ├── bitutil   // 该包实现快速位操作
│   ├── compiler  // 包装了Solity编译器可执行文件
│   ├── fdlimit
│   ├── hexutil   // 以0x为前缀的十六进制编码 
│   ├── math
│   └── mclock
├── consensus     // 实现了不同以太共识引擎
│   ├── bftcosi
│   │   └── cosi
│   ├── clique    // 实现了权威共识引擎 
│   ├── ethash    // 发动机工作的共识ethash证明 
│   └── misc
├── console
│   └── testdata
├── containers
│   └── docker
│       ├── develop-alpine
│       ├── develop-ubuntu
│       ├── master-alpine
│       └── master-ubuntu
├── contracts
│   ├── chequebook  // ‘支票薄’以太智能合约 
│   └── ens
├── core            // 核心部分，它包含账户、区块、创世块、区块链、transaction、bloom的定义以及区块如何验证、如何加入链以及transaction如何使用vm执行也就是智能合约的执行都在这里完成。 
│   ├── asm         // 汇编和反汇编接口
│   ├── bloombits   // Bloom过滤批量数据
│   ├── rawdb
│   ├── state       // 封装在以太状态树之上的一种缓存结构
│   ├── types       // 以太合约支持的数据类型
│   └── vm          // 以太虚拟机
│       └── runtime // 一种用于执行EVM代码的基本执行模型
├── crypto          // 加密工具包含不限于hash算法、ECC算法等
│   ├── bn256       // 最优的ATE配对在256位Barreto-Naehrig曲线上 
│   │   ├── cloudflare  // 在128位安全级别上的特殊双线性组
│   │   └── google   // 在128位安全级别上的特殊双线性组
│   ├── ecies
│   ├── randentropy
│   ├── secp256k1    // 封装比特币secp256k1的C库
│   └── sha3         // Sha-3固定输出长度散列函数 and 由FIPS-202定义的抖动变量输出长度散列函数
├── dashboard
│   └── assets
│       ├── components
│       └── types
├── docs
├── eth             // 以太坊协议 
│   ├── downloader  // 手动全链同步 
│   ├── fetcher     // 基于块通知的同步 
│   ├── filters     // 用于区块，交易和日志事件的过滤 
│   ├── gasprice    
│   └── tracers     // 收集JavaScript交易追踪 
├── ethclient       // 以太坊RPC AIP客户端 
├── ethdb           // 提供了数据源实现内存以及leveldb，并使用数据源扩展了不同实现，这包含缓存数据源、链数据源，依据于此又封装出读写缓存、异步读写缓存以及链存储相关的数据源实现。定义了如何使用datasource存储block、transaction，换句话说就是block、transaction的存储数据结构
├── ethstats        // 网络统计报告服务 
├── event           // 处理实时事件的费用 
│   └── filter      // 事件过滤
├── internal        
│   ├── build       
│   ├── cmdtest
│   ├── debug       // 调试接口Go运行时调试功能 
│   ├── ethapi      // 常用的以太坊API函数 
│   ├── guide       // 小测试套件，以确保开发指南工作中的代码段正常运行 
│   ├── jsre        // JavaScript执行环境 
│   │   └── deps    // 控制台JavaScript依赖项Go嵌入 
│   └── web3ext     // geth确保web3.js延伸
├── les             // 轻量级Ethereum子协议 
│   └── flowcontrol // 客户端流程控制机制 
├── light           // 客户端实现按需检索能力的状态和链对象
├── log             // 输出日志 
│   └── term
├── metrics         // Coda Hale度量库的Go端口 
│   ├── exp         // 表达式相关操作 
│   ├── influxdb
│   └── librato
├── miner           // 以太坊块创建和挖矿 
├── mobile          // geth的移动端API
├── node            // 设置多维接口节点
├── p2p             // p2p网络协议 
│   ├── discover    // 节点发现协议 
│   ├── discv5      // RLPx v5主题相关的协议 
│   ├── enr         // 实现EIP-778中的以太坊节点记录 
│   ├── nat         // 提供网络端口映射协议的权限 
│   ├── netutil     // 网络包拓展 
│   ├── protocols   // p2p拓展 
│   └── simulations // 实现模拟p2p网络 
├── params
├── rlp             // RLP系列化格式 
├── rpc             // 通过网络或者I/O链接来访问接口
├── signer
│   ├── core
│   ├── rules
│   │   └── deps
│   └── storage
├── swarm
│   ├── api
│   │   ├── client
│   │   └── http
│   ├── bmt
│   ├── dev
│   ├── fuse
│   ├── grafana_dashboards
│   ├── log
│   ├── metrics
│   ├── multihash
│   ├── network
│   ├── pot
│   ├── pss
│   ├── services
│   ├── state
│   ├── storage
│   └── testutil
├── tests             // 以太坊JSON测试 
├── trie              // Merkle Patricia树实现
├── vendor
│   ├── github.com
│   ├── gopkg.in
│   │   └── check.v1  // Go更深的测试 
└── whisper
    ├── mailserver
    ├── shhclient
    ├── whisperv5
    └── whisperv6
```