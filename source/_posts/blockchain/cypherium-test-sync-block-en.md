---
title: Cypherium's synchronous block test operation steps
date: 2018-08-15
tags: [Blockchain,Cypherium]
categories: 
  - Blockchain
---

转载请标注原文地址：https://lilyssh.cn/blockchain/cypherium-test-sync-block-en/
# 前言  
Cypherium's synchronous block test operation steps.
<!-- more -->
1. put cypherium_internal.pem in a directory,for example,~.

2. `vim ~/.ssh/config` file.Add alias for the ssh remote connection.

```
  Host *
      ServerAliveInterval 60
  Host 119
      HostName 18.221.58.119
      User ubuntu
      IdentityFile    ~/cypherium_internal.pem
  Host 200
      HostName 18.216.16.200
      User ubuntu
      IdentityFile    ~/cypherium_internal.pem
```
3. `ssh 119`,connection AWS node 18.221.58.119.

```
ubuntu@ip-172-31-31-48:~$
$ tree
.
├── genesis.json
├── group.toml
├── Cypherium
│   ├── private.toml
│   └── public.toml
└── cypher_db
    ├── cypher.ipc
```
4. clean  

```
make clean
```
5. Compile  

```
make cypher
```
4. Generate node information 

```
./build/bin/cypher setup
```
You need to enter the ip: port,Use kyber's Ed25519 algorithm to generate `public.toml` and `private.toml`.Save in the default path. Currently there is no ip discovery, so you need to manually splicing the public key `~/Library/Application\ Support/cypher/public.toml` (Mac default path) for simulation testing. The node name can be customized, such as node_01_name.
![](https://resource.lilyssh.cn/pic/cypher%20setup_.png)
5. 同理，再生成第二个节点。

6. 把第一、二个节点的public.toml，拼接到~/workspace/test/group.toml中。

![](https://resource.lilyssh.cn/pic/group.toml.png)
7. 初始化 两个节点的创世区块。  

第一个节点：  
```
./build/bin/cypher --datadir "~/workspace/test/node_01/data" init ~/workspace/test/genesis.json
```
第二个节点:  
```
./build/bin/cypher --datadir "~/workspace/test/node_02/data" init ~/workspace/test/genesis.json
```
genesis.json中内容可参考：
```
{
  "config": {
        "chainId": 123666,
        "homesteadBlock": 0,
        "eip155Block": 0,
        "eip158Block": 0
    },
  "alloc"      : {},
  "coinbase"   : "0x0000000000000000000000000000000000000000",
  "difficulty" : "0x20000",
  "extraData"  : "",
  "gasLimit"   : "0x2fefd8",
  "nonce"      : "0x0000000000000042",
  "mixhash"    : "0x0000000000000000000000000000000000000000000000000000000000000000",
  "parentHash" : "0x0000000000000000000000000000000000000000000000000000000000000000",
  "timestamp"  : "0x00"
}
```
8. 启动第一个节点。

```
./build/bin/cypher --onetdir ~/workspace/test/node_01/private.toml --datadir ~/workspace/test/node_01/data --publickeydir ~/workspace/test/group.toml --networkid 123666 --port 7010 --rpcport 8510 --rpc --rpccorsdomain "*" --rpcaddr 0.0.0.0 --rpcapi eth,web3,personal --verbosity 5 2> ~/workspace/test/node_01/output2.log
```
9. 启动第一个节点的js交互窗口(用上一步成的ipc文件)：

```
./build/bin/cypher attach ~/workspace/test/node_01/data/cypher.ipc
```
10. 启动第二个节点。

```
./build/bin/cypher --onetdir ~/workspace/test/node_02/private.toml --datadir ~/workspace/test/node_02/data --publickeydir ~/workspace/test/group.toml --networkid 123666 --port 7020 --rpcport 8520 --rpc --rpccorsdomain "*" --rpcaddr 0.0.0.0 --rpcapi eth,web3,personal --verbosity 5 2> ~/workspace/test/node_02/output2.log
```
11. 启动第二个节点的js交互窗口(用上一步成的ipc文件)。

```
./build/bin/cypher attach ~/workspace/test/node_02/data/cypher.ipc
```
![](https://resource.lilyssh.cn/pic/attach%20node_.jpg)
12. 从此步骤开始，都将在js交互涌窗口执行。查看第一个节点的信息。

```
 admin.nodeInfo
```
13. 在第二个节点中，创建用户，保存挖矿奖励。

```
 personal.newAccount("password")
```
14. 在第二个节点中，加入第一个节点。就是把cnode中的内容粘进addPeer中。

```
 admin.addPeer("cnode")
```
15. 在第一个节点中，验证一下是否已加入。

```
 net
```
16. 主节点调用开始同步命令，从交易池里取tx执行，并打包生成txBlock，发给其他委员会成员做bftcosi共识。会看到两个节点的信息都在滚动。

```
bftcosi.start()
```
17. 停止共识。

```
bftcosi.stop()
```
18. 查看下两个节点的区块数量是否相同。

![](https://resource.lilyssh.cn/pic/txBlockNumber.png)

19. (不时需要)解锁账户

```
personal.unlockAccount("需解锁账户")
```
![](https://resource.lilyssh.cn/pic/unlockAccount.png)
20. 查看交易池状态，pending为待确认的交易数量。
```
 txpool.status
```
会显示
```
{
  pending: 0,
  queued: 0
}
```


19. 退出：

![](https://resource.lilyssh.cn/pic/killall.png)
