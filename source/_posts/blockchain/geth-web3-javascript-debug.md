---
title: Go Ethereum 以太坊 web3.js 控制台 调试
date: 2018-08-05
tags: [Blockchain,区块链,以太坊]
categories: 
  - Blockchain
---
转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/82911363  
# 前言  
最近要根据以太坊，做个数字货币，需要修改一些命令，就看了一下[web3.js](https://github.com/ethereum/web3.js)。  
每次改完，都需要打包，放到[Go Ethereum](https://github.com/ethereum/go-ethereum)里测试，非常麻烦，就研究了一波怎么调试和怎么使用测试用例。
<!-- more -->
#  一、从官网寻找测试方法
1、从web3.js的github地址页面，看[README.md](https://github.com/ethereum/web3.js/blob/develop/README.md)，能看到测试命令：
```
npm test
```
2、从[package.json](https://github.com/ethereum/web3.js/blob/develop/package.json)中可以看到`npm test`的测试方式：  
```
"scripts": {
    "build": "gulp",
    "watch": "gulp watch",
    "lint": "jshint *.js lib",
    "test": "mocha; jshint *.js lib",
    "test-coveralls": "istanbul cover _mocha -- -R spec && cat coverage/lcov.info | coveralls --verbose"
  },
```
看到web3.js是用`mocha`测试的。  
#  二、使用[WebStorm](http://www.jetbrains.com/webstorm/)调试[web3.js](https://github.com/ethereum/web3.js) 
1、点击`Add Configuration`，添加一个测试配置。  
![](https://resource.lilyssh.cn/pic/add%20configuration.png)
2、选择Mocha：  
![](https://resource.lilyssh.cn/pic/add%20mocha.png)
3、配置mocha：  
![](https://resource.lilyssh.cn/pic/config%20mocha.png)
4、debug：  
![](https://resource.lilyssh.cn/pic/mocha%20debug.png)

接下来就可以愉快的调试啦！
