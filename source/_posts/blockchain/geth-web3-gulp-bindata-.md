---
title: Go Ethereum 以太坊 web3.js 打包编译发布过程
date: 2018-08-09
tags: [Blockchain,区块链,以太坊]
categories: 
  - Blockchain
---
转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/82911406  
# 前言
以太坊的js交互是靠[web3.js](https://github.com/ethereum/web3.js)调用的。  
改完后，需要打包，放到[Go Ethereum](https://github.com/ethereum/go-ethereum)里测试,以下就是打包编译过程说明。
web3.js版本：v0.20.6。
<!-- more -->

1. 安装cnpm

```
npm install -g cnpm --registry=https://registry.npm.taobao.org
```
2. 在`web3.js`的根路径下 安装`web3.js`的依赖

```
$ cnpm install
```
3. 安装`gulp`打包命令

```
npm install gulp -g
```
4. 在`web3.js`的根路径下 执行`gulp`

```
gulp
```
5. 把`dist/web3.js`文件拷贝到项目中

```
cp dist/web3.js ../go-cypherium/internal/jsre/deps
```
6. 安装`go-bindata`(我是在~目录下执行的)

```
go get -u github.com/jteeuwen/go-bindata/...
```
7. 复制 go-bindata文件 到 go的安装目录的bin下。

```
cp ~/go/bin/go-bindata  ~/install/go/bin
```
8. 在cypherium_private/go-cypherium/internal/jsre/deps下执行deps.go的倒数第二行,会看到bindata.go已变蓝色，大功告成！

```
go-bindata -nometadata -pkg deps -o bindata.go bignumber.js web3.js
```