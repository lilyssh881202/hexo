---
title: 高并发架构实战(五) Spring Boot 集成 spring-cloud-config  
date: 2018-01-05
tags: [架构,高并发架构实战,Spring Boot,Spring Cloud Config]
categories: 
  - 架构
---
转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/82754702  

Spring Boot 2.0.4 集成 [spring-cloud-config](http://cloud.spring.io/spring-cloud-config) 2.0.1。
[项目源码地址](https://gitee.com/lilyssh/high-concurrency)
<!-- more -->
# 一、简介
Spring Cloud Config为服务端和客户端提供了分布式系统的外部化配置支持。配置服务器为各应用的所有环境提供了一个中心化的外部配置。spring cloud config支持配置文件放在在配置服务的内存中，也支持放在远程Git仓库里。
# 二、为什么用
在分布式系统中，由于服务数量庞大，如果各个项目管理各自的配置文件，修改起来会非常困难。
# 三、有什么好处
- 方便服务配置文件统一管理，实时更新，且易于部署、维护。方便切换环境。
它实现了对服务端和客户端对Spring Environment和PropertySource抽象的映射，所以它除了适用于Spring构建的应用程序，也可以在任何其他语言运行的应用程序中使用。
# 四、适用场景
# 五、使用方法
引入spring cloud config后，外部配置文件就可以集中放置在一个git仓库里，再新建一个config server，用来管理所有的配置文件，需要更改配置时，只需要在本地更改后，推送到远程仓库，所有的服务都可以通过config server来获取配置文件，这时每个服务就相当于配置服务的客户端config client,为了保证系统的稳定，配置服务端config server可以进行集群部署，即使某一个实例，因为某种原因不能提供服务，也还有其他的实例保证服务的继续进行。
## 1、配置config服务端
新建配置服务项目，如config-server。  
### （1）在config-server项目中添加依赖
```
<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-config</artifactId>
            <version>2.0.1.RELEASE</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>

<dependencies>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-config-server</artifactId>
    </dependency>
</dependencies>
```
### （2）在启动类上加注解@EnableConfigServer
```java
package cn.lilyssh.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigApplication.class, args);
	}
}
```
### （3）创建配置仓库
在github上新建一个空项目：https://gitee.com/lilyssh/config-repo 作为配置仓库。
在config-repo项目中新建一个公用的配置文件application-dev.yml，内容如下：
```yml
logging:
  level:
    cn.lilyssh: info

  dubbo:
    registry:
      address: zookeeper://ssh.qianxunclub.com:2181
```
再新建一个数据库配置文件mysql-dev.yml,内容如下：
```yml
spring:
  datasource:
    druid:
      url: jdbc:mysql://db.qianxunclub.com:3306/demo

mybatis-plus:
  global-config:
    db-config:
      id-type: AUTO
```
### （4）在config-server项目`application.yml`中配置仓库信息
```yml
spring:
  cloud:
    config:
      server:
        git:
          uri: https://gitee.com/lilyssh/config-repo.git
          basedir: config/data
          search-paths: /**
```
- uri：上一步创建的配置仓库地址。
- basedir: 把远程仓库数据下载到本地的目录。由于源码中写着会访问到父级目录，所以此处需要设置为两级目录。
- search-paths: 要下载的远程仓库的目录。
### （5）测试访问远程仓库配置  
在浏览器中，输入地址http://localhost:8888/{app}/{profile} 由于刚在远程仓库建的配置文件名为application-dev.yml，所以需要访问
http://localhost:8888/application/dev 进行测试，会看到：
```json
{
    "name":"application",
    "profiles":[
        "dev"
    ],
    "label":null,
    "version":"9fdeb7331e3e78da436b0a404707c1873fe72076",
    "state":null,
    "propertySources":[
        {
            "name":"https://gitee.com/lilyssh/config-repo.git/application-dev.yml",
            "source":{
                "logging.level.cn.lilyssh":"info",
                "spring.dubbo.registry.address":"zookeeper://ssh.qianxunclub.com:2181"
            }
        }
    ]
}
```
证明配置服务中心可以从远程程序获取配置信息，http请求地址和资源文件映射如下:  
可参考`/{application}/{profile}[/{label}]`
- /{application}-{profile}.yml
- /{label}/{application}-{profile}.yml
- /{application}-{profile}.properties
- /{label}/{application}-{profile}.properties
## 2、配置config客户端
### （1）在order-provider项目中添加依赖
```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-config</artifactId>
    </dependency>
</dependencies>

<dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-config</artifactId>
            <version>2.0.1.RELEASE</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```
### （2）新建配置文件`bootstrap.yml`
```yml
spring:
  cloud:
    config:
      name: application,mysql
      profile: dev
      uri: http://config.qianxunclub.com
```
- name: 配置文件名的前半部分,如“db-dev.yml”中的db。如果是多个，以逗号分隔。
- profile: 配置文件名的后半部分，如"db-dev.yml中得dev。
- uri: 配置服务地址。

## 3、测试
![](https://resource.lilyssh.cn/pic/add_order_result.png)
大功告成！