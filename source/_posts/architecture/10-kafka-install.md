---
title: 高并发架构实战(十) Kafka的安装与使用
date: 2018-01-10
tags: [架构,高并发架构实战,Kafka]
categories: 
  - 架构
---

转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/83346201  

## 1、下载Kafka
下载地址：https://kafka.apache.org/downloads
```
wget http://mirrors.hust.edu.cn/apache/kafka/2.0.0/kafka_2.11-2.0.0.tgz 
```
解压：
```
tar -zxvf kafka_2.11-2.0.0.tgz

cd /usr/local/kafka_2.11-2.0.0/
```
## 2、修改配置
把config/server.properties中advertised.listeners设置为服务器的IP，可使用`ping`命令查看IP。
```
advertised.listeners=PLAINTEXT://120.25.173.32:9092
```
## 3、启动
### 启动zookeeper
```
bin/zookeeper-server-start.sh -daemon config/zookeeper.properties
```
### 启动kafka
```
bin/kafka-server-start.sh  config/server.properties
```
## 4、创建TOPIC
```
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
```
查看 topic 列表：
```
bin/kafka-topics.sh --list --zookeeper localhost:2181
```
## 5、发送消息
```
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test
```
## 6、消费消息
可以在另一台机子上，启动kafka后，执行消费消息的命令：
```
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
```
--from-beginning 代表从头开始消费消息。
在生产端发送消息，可以在消费端收到消息。

大功告成！