---
title: 高并发架构实战(七) Elasticsearch的安装与使用
date: 2018-01-07
tags: [架构,高并发架构实战,Elasticsearch]
categories: 
  - 架构
---

转载请标注原文地址：https://lilyssh.cn/architecture/7-elasticsearch-install/
# 一、简介
Elasticsearch是一个基于Apache Lucene(TM)的开源搜索引擎。Elasticsearch使用Java开发并使用Lucene作为其核心来实现所有索引和搜索的功能，它的目的是通过简单的RESTful API来隐藏Lucene的复杂性，从而让全文搜索变得简单。它是分布式的实时文件存储，每个字段都被索引并可被搜索。分布式的实时分析搜索引擎。可以扩展到上百台服务器，处理PB级结构化或非结构化数据。
<!-- more -->
# 二、优势
上手Elasticsearch非常容易。它提供了许多合理的缺省值，并对初学者隐藏了复杂的搜索引擎理论。它开箱即用（安装即可使用），只需很少的学习既可在生产环境中使用。
# 三、安装Elasticsearch
因为安全问题，Elasticsearch不允许root用户直接运行，否则会报错：`can not run elasticsearch as root`。即使事后再把Elasticsearch拥有权赋给新建用户，也会报错：
```
错误: 找不到或无法加载主类 org.elasticsearch.tools.java_version_checker.JavaVersionChecker
原因: java.lang.ClassNotFoundException: org.elasticsearch.tools.java_version_checker.JavaVersionChecker
```
## 1、创建新用户。
```
adduser admin  # 创建admin用户

su admin       # 切换到admin用户
```
## 2、下载并解压Elasticsearch
从[Elasticsearch官网](https://www.elastic.co/cn/downloads/elasticsearch) 下载最新版本的Elasticsearch，并解压。
```
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.4.2.tar.gz

tar -zxvf elasticsearch-6.4.2.tar.gz

cd elasticsearch-6.4.2 
```
## 3、后台运行Elasticsearch
```
./bin/elasticsearch -d
```
## 4、验证是否启动成功
curl http://localhost:9200
```

{
  "name" : "LwWH-xd",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "8HsSQm_NSPOSMpbwSIXDZw",
  "version" : {
    "number" : "6.4.2",
    "build_flavor" : "default",
    "build_type" : "tar",
    "build_hash" : "04711c2",
    "build_date" : "2018-09-26T13:34:09.098244Z",
    "build_snapshot" : false,
    "lucene_version" : "7.4.0",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
```

# 四、安装Elasticsearch head
## 1、下载并安装Elasticsearch head
```
git clone git://github.com/mobz/elasticsearch-head.git

cd elasticsearch-head

npm install

npm run start
```
由于`npm`太慢，可换成`cnpm`。
## 2、配置head

## 3、测试
打开 http://localhost:9100/

大功告成！