---
title: 高并发架构实战(三) Spring Boot 集成 mybatis-plus  
date: 2018-01-03
tags: [架构,Spring Boot,MyBatis Plus,高并发架构实战]
categories: 
  - 架构
---
转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/82753716  

Spring Boot 2.0.4 集成 [mybatisplus-spring-boot-starter 1.0.5](http://mp.baomidou.com/) 。[项目源码地址](https://gitee.com/lilyssh/high-concurrency)  
<!-- more -->
# 1. 初始化工程
工程 user-provider的结构为：
```
$ tree -I target
.
├── pom.xml
└── src
    └── main
        ├── java
        │   └── cn
        │       └── lilyssh
        │           └── user
        │               └── provider
        │                   ├── UserProviderApplication.java
        │                   ├── dao
        │                   │   ├── entity
        │                   │   │   └── UserEntity.java
        │                   │   ├── mapper
        │                   │   │   └── UserMapper.java
        │                   │   └── repository
        │                   │       └── UserRepository.java
        │                   └── service
        │                       └── UserService.java
        └── resources
            └── application.yml

```
# 2. 添加依赖
在`user-provider`工程中打开`pom.xml`加入以下依赖。  
引入`mybatis-plus-boot-starter`依赖：
```xml
<dependency>
	<groupId>com.baomidou</groupId>
	<artifactId>mybatis-plus-boot-starter</artifactId>
	<version>3.0.1</version>
</dependency>
```
# 3. 编码
(1) 编写Mapper类，要加`@Mapper`，继承`BaseMapper<T>`。
```java
package cn.lilyssh.user.provider.dao.mapper;

import cn.lilyssh.user.provider.dao.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
    
}
```
(2) 编写repository类，继承`ServiceImpl<xxMapper, xxEntity>`
```java
package cn.lilyssh.user.provider.dao.repository;

import cn.lilyssh.user.provider.dao.entity.UserEntity;
import cn.lilyssh.user.provider.dao.mapper.UserMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserRepository extends ServiceImpl<UserMapper, UserEntity> {

}
```
(3) 添加接口实现类，调用`mybatis plus`提供的方法。
```java
package cn.lilyssh.user.provider.service;

import cn.lilyssh.user.api.model.response.UserQueryResp;
import cn.lilyssh.user.api.service.UserServiceApi;
import cn.lilyssh.user.provider.dao.entity.UserEntity;
import cn.lilyssh.user.provider.dao.repository.UserRepository;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Component
@AllArgsConstructor
public class UserService  implements UserServiceApi {

    private UserRepository userRepository;

    @Override
    public List<UserQueryResp> list(){
        QueryWrapper<UserEntity> qw = new QueryWrapper<>();
        List<UserEntity> userEntityList = userRepository.list(qw);
        List<UserQueryResp> resultList=new ArrayList<>();
        userEntityList.forEach(userEntity->{
            UserQueryResp  userQueryResp=new UserQueryResp();
            BeanUtils.copyProperties(userEntity,userQueryResp);
            resultList.add(userQueryResp);
        });
        return resultList;
    }
}

```
# 4. 测试
(1) 使用`telnet`模拟调用`dubbo`服务,端口为user-provider.xml里配置的dubbo端口，默认20880。
```
telnet localhost 20880
```
如果连接成功，会看到
```
Trying ::1...
Connected to localhost.
Escape character is '^]'.

dubbo>
```
(2) 使用`ls`查看所有服务。
```
dubbo>ls
cn.lilyssh.user.api.service.UserServiceApi
```
(3) 使用`cd`进入到com.qianxunclub.demo.dubbo.DemoService中,并使用`ls`查看服务里的方法。
```
dubbo>cd cn.lilyssh.user.api.service.UserServiceApi
Used the cn.lilyssh.user.api.service.UserServiceApi as default.
You can cancel default service by command: cd /
dubbo>ls
Use default service cn.lilyssh.user.api.service.UserServiceApi.

list
dubbo>
```
(4) 使用`invoke`模拟客户端调用服务。
```
dubbo>invoke list()
```
会看到：
```
Use default service cn.lilyssh.user.api.service.UserServiceApi.
[{"age":0,"id":1,"idType":0,"phone":0,"sex":0,"status":0,"userName":"Tom","uuid":"3"},{"age":0,"id":2,"idType":0,"phone":0,"sex":0,"status":0,"userName":"Mike","uuid":"4"}]
elapsed: 83 ms.
```
大功告成！