---
title: 高并发架构实战(九) Spring Boot 集成 Swagger2  
date: 2018-01-09
tags: [架构,高并发架构实战,Spring Boot,Swagger2]
categories: 
  - 架构
---
转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/82945876  

Spring Boot 2.0.4 集成 [swagger](https://swagger.io/tools/swagger-ui/) 2.9.2。
[项目源码地址](https://gitee.com/lilyssh/high-concurrency)
<!-- more -->
# 一、简介
Swagger是一款Restful接口的文档在线自动生成的软件，也能进行功能测试。   
# 二、好处
- 方便开发人员单元测试，提高开发效率。
# 三、使用方法
先看下目录结构
```
~/workspace/gitee/high-concurrency on master ⌚ 12:52:03
$ tree -I target
.
├── README.md
├── common
│   ├── pom.xml
│   └── src
│       └── main
│           ├── java
│           │   └── cn
│           │       └── lilyssh
│           │           └── common
│           │               └── swagger
│           │                   ├── Swagger2Config.java
│           │                   └── SwaggerProperties.java
│           └── resources
│               └── META-INF
│                   └── spring.factories
├── order
│   ├── order-api
│   │   ├── pom.xml
│   │   └── src
│   │       └── main
│   │           └── java
│   │               └── cn.lilyssh.order.api
│   │                   ├── model
│   │                   │   ├── request
│   │                   │   │   ├── OrderInsertReq.java
│   │                   │   │   └── OrderQueryReq.java
│   │                   │   └── response
│   │                   │       └── OrderQueryResp.java
│   │                   └── service
│   │                       └── OrderServiceApi.java
│   └── order-consumer
│       ├── pom.xml
│       └── src
│           └── main
│               ├── java
│               │   └── cn
│               │       └── lilyssh
│               │           └── order
│               │               └── consumer
│               │                   ├── OrderConsumerApplication.java
│               │                   ├── controller
│               │                   │   └── OrderController.java
│               │                   └── service
│               │                       └── OrderService.java
│               └── resources
│                   ├── application.yml
│                   └── bootstrap.yml
└── pom.xml

```
## 1、common项目
### （1）添加swagger依赖
```
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.9.2</version>
</dependency>
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.9.2</version>
</dependency>
```
### （2）新建Swagger的配置类
```java
package cn.lilyssh.common.swagger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@EnableConfigurationProperties({SwaggerProperties.class})
public class Swagger2Config {

    @Autowired
    private SwaggerProperties swaggerProperties;
    /**
     * 添加摘要信息(Docket)
     */
    @Bean
    public Docket controllerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(true)
                .apiInfo(new ApiInfoBuilder()
                        .title(swaggerProperties.getTitle())
                        .description(swaggerProperties.getDescription())
                        .contact(new Contact(swaggerProperties.getAuthor(), swaggerProperties.getUrl(), swaggerProperties.getEmail()))
                        .version(swaggerProperties.getVersion())
                        .build())
                .select()
                .apis(RequestHandlerSelectors.basePackage(swaggerProperties.getBasePackage()))
                .paths(PathSelectors.any())
                .build();
    }
}
```

```java
package cn.lilyssh.common.swagger;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "swagger")
public class SwaggerProperties {
    private boolean enable;
    private String title;
    private String description;
    private String author;
    private String email;
    private String version;
    private String basePackage;
    private String url;
}
```
### （3）配置spring扫描路径
`spring.factories`中内容为
```properties
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
  cn.lilyssh.common.exception.ExceptionAdviceHandler,\
  cn.lilyssh.common.swagger.Swagger2Config
```
换行要用\。
## 2、order-consumer项目
### （1）添加common依赖
```
<dependency>
  <groupId>cn.lilyssh</groupId>
  <artifactId>common</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```
### （2）在启动类中添加@EnableSwagger2注解
```java
package cn.lilyssh.order.consumer;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 开启在线接口文档
 */
@EnableSwagger2
@SpringBootApplication
@EnableDubboConfiguration
public class OrderConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderConsumerApplication.class, args);
	}
}
```
### （3）在application.yml中添加配置
```yml
swagger:
  title : lilyssh_电商系统_接口文档
  description : 用于网上购物的订单模块
  author : lily
  url : lilyssh.cn
  email : 1643995620@qq.com
  version : 版本号:1.0
  basePackage : cn.lilyssh.order.consumer.controller
```
### （4）在controller中添加注解
```java
package cn.lilyssh.order.consumer.controller;

import cn.lilyssh.common.result.Result;
import cn.lilyssh.common.validate.ValidateGroup;
import cn.lilyssh.order.api.model.request.OrderInsertReq;
import cn.lilyssh.order.api.model.request.OrderQueryReq;
import cn.lilyssh.order.consumer.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(description = "订单接口")
@RestController
@AllArgsConstructor
@RequestMapping("/order")
public class OrderController {

    private OrderService orderService;

    @ApiOperation("获取所有订单")
    @GetMapping
    public Result orderList(OrderQueryReq orderQueryReq){
        return orderService.orderList(orderQueryReq);
    }

    @ApiOperation("下单")
    @PostMapping
    public Result save(@RequestBody @Validated(value = ValidateGroup.Insert.class) OrderInsertReq orderInsertReq){
        return orderService.save(orderInsertReq);
    }
}
```
## 3、order-api项目
### （1）在实体类中添加注解
```java
package cn.lilyssh.order.api.model.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.properties.BaseIntegerProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@ApiModel(description = "订单查询请求数据")
public class OrderQueryReq implements Serializable {

    @ApiModelProperty(value = "订单ID")
    private Integer id;
    @ApiModelProperty(value = "用户ID")
    private Integer userId;
    private String userUuid;
    private BigDecimal payment;
    private Integer payType;
    private BigDecimal postFee;
    private Integer status;
    private Date createTime;
    private Date updateTime;
    private Date payTime;
    private Date cosignTime;
    private Date endTime;
    private Date closeTime;
    private String shippingName;
    private String shippingCode;
}
```
访问`http://localhost:1111/swagger-ui.html`，报错:`Illegal DefaultValue null for parameter type integer.`，解决办法：
实体类中，Integer类型的属性加@ApiModelProperty时，必须要给example参数赋值，且值必须为数字类型。
```java
@Data
@ApiModel(description = "订单查询请求数据")
public class OrderQueryReq implements Serializable {
    @ApiModelProperty(value = "订单ID",example = "123")
    private Integer id;
}
```
异常分析过程 请参考：[swagger2异常:java.lang.NumberFormatException:For input string:""](https://lilyssh.cn/exception/3-swagger-NumberFormatException/)
再次访问
![](https://resource.lilyssh.cn/pic/swagger.png)
大功告成！