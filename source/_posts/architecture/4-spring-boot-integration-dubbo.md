---
title: 高并发架构实战(四) Spring Boot 集成 dubbo-spring-boot  
date: 2018-01-04
tags: [架构,高并发架构实战,Spring Boot,Dubbo]
categories: 
  - 架构
---
转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/82753906  

Spring Boot 2.0.4 集成 dubbo-spring-boot-starter 2.0.0。[项目源码地址](https://gitee.com/lilyssh/high-concurrency)
<!-- more -->
# 一. 初始化工程
创建一个空的 Spring Boot 工程
>TIP
>
>可以使用 [Spring Initializr](https://start.spring.io/) 快速初始化一个 Spring Boot 工程 
 
在high-concurrency/user文件夹下，创建工程：
（1）user-provider  &nbsp;&nbsp;&nbsp;&nbsp;//服务提供者  
（2）user-api  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//接口  
（3）user-consumer  &nbsp;//服务消费者

```
~/workspace/gitee/high-concurrency/user on  master ⌚ 12:47:10
$ tree -I target
.
├── pom.xml
├── user-api
│   ├── pom.xml
│   └── src
│       └── main
│           └── java
│               └── cn
│                   └── lilyssh
│                       └── user
│                           └── api
│                               ├── model
│                               │   ├── request
│                               │   │   ├── UserInsertReq.java
│                               │   │   └── UserQueryReq.java
│                               │   └── response
│                               │       └── UserQueryResp.java
│                               └── service
│                                   └── UserServiceApi.java
├── user-consumer
│   ├── pom.xml
│   └── src
│       └── main
│           ├── java
│           │   └── cn
│           │       └── lilyssh
│           │           └── user
│           │               └── consumer
│           │                   ├── UserConsumerApplication.java
│           │                   ├── controller
│           │                   │   └── UserController.java
│           │                   └── service
│           │                       └── UserService.java
│           └── resources
│               └── application.yml
└── user-provider
    ├── pom.xml
    └── src
        └── main
            ├── java
            │   └── cn
            │       └── lilyssh
            │           └── user
            │               └── provider
            │                   ├── UserProviderApplication.java
            │                   ├── dao
            │                   │   ├── entity
            │                   │   │   └── UserEntity.java
            │                   │   ├── mapper
            │                   │   │   └── UserMapper.java
            │                   │   └── repository
            │                   │       └── UserRepository.java
            │                   └── service
            │                       └── UserService.java
            └── resources
                └── application.yml
```
# 二、在user-api工程中添加接口
(1) 添加实体类
```java
package cn.lilyssh.user.api.model.request;

import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@Data
public class UserQueryResp implements Serializable {
    private int id;
    private String uuid;
    private String userName;
    private String password;
    private String realName;
    private int sex;
    private int age;
    private int phone;
    private String email;
    private int status;
    private String lastLoginIp;
    private Date lastLoginTime;
    private int idType;
    private String idNumber;
    private String address;
}
```
(2) 添加接口
```java
package cn.lilyssh.user.api.service;

import cn.lilyssh.user.api.model.response.UserQueryResp;
import java.util.List;

public interface UserServiceApi {
    List<UserQueryResp> list();
}
```
# 三、发布dubbo服务（user-provider）
## 1. 添加依赖
```xml
<dependency>
	<groupId>com.alibaba.spring.boot</groupId>
	<artifactId>dubbo-spring-boot-starter</artifactId>
	<version>RELEASE</version>
	<scope>compile</scope>
</dependency>
<dependency>
	<groupId>com.101tec</groupId>
	<artifactId>zkclient</artifactId>
	<version>0.10</version>
</dependency>
<dependency>
	<groupId>com.alibaba</groupId>
	<artifactId>fastjson</artifactId>
	<version>1.2.49</version>
</dependency>
```
## 2. dubbo配置
（1）在src/main/resources下新建文件`application.yml`，配置dubbo ：
```yml
spring:
  application:
    name: user-provider
  dubbo:
    registry: zookeeper://ssh.qianxunclub.com:2181
```
(2) 在`spirng boot`启动类中，添加@EnableDubboConfiguration注解启用dubbo:
```java
package cn.lilyssh.user.provider;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubboConfiguration
public class UserProviderApplication {
	public static void main(String[] args) {
		SpringApplication.run(UserProviderApplication.class, args);
	}
}
```
(3) 创建接口实现类
```java
package cn.lilyssh.user.provider.service;

import cn.lilyssh.user.api.model.response.UserQueryResp;
import cn.lilyssh.user.api.service.UserServiceApi;
import cn.lilyssh.user.provider.dao.entity.UserEntity;
import cn.lilyssh.user.provider.dao.repository.UserRepository;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Component
@AllArgsConstructor
public class UserService  implements UserServiceApi {

    private UserRepository userRepository;

    @Override
    public List<UserQueryResp> list(){
        QueryWrapper<UserEntity> qw = new QueryWrapper<>();
        List<UserEntity> userEntityList = userRepository.list(qw);
        List<UserQueryResp> resultList=new ArrayList<>();
        userEntityList.forEach(userEntity->{
            UserQueryResp  userQueryResp=new UserQueryResp();
            BeanUtils.copyProperties(userEntity,userQueryResp);
            resultList.add(userQueryResp);
        });
        return resultList;
    }
}
```
## 3. 启动项目，模拟测试调用dubbo服务
(1) 使用`telnet`模拟调用`dubbo`服务,端口为user-provider.xml里配置的dubbo端口，默认20880。
```
telnet localhost 20880
```
如果连接成功，会看到
```
Trying ::1...
Connected to localhost.
Escape character is '^]'.

dubbo>
```
(2) 使用`ls`查看所有服务。
```
dubbo>ls
cn.lilyssh.user.api.service.UserServiceApi
```
(3) 使用`cd`进入到com.qianxunclub.demo.dubbo.DemoService中,并使用`ls`查看服务里的方法。
```
dubbo>cd cn.lilyssh.user.api.service.UserServiceApi
Used the cn.lilyssh.user.api.service.UserServiceApi as default.
You can cancel default service by command: cd /
dubbo>ls
Use default service cn.lilyssh.user.api.service.UserServiceApi.

list
dubbo>
```
(4) 使用`invoke`模拟客户端调用服务。
```
dubbo>invoke list()
Use default service cn.lilyssh.user.api.service.UserServiceApi.
[{"age":0,"id":1,"idType":0,"phone":0,"sex":0,"status":0,"userName":"Tom","uuid":"3"},{"age":0,"id":2,"idType":0,"phone":0,"sex":0,"status":0,"userName":"Mike","uuid":"4"}]
elapsed: 334 ms.
dubbo>
```
# 四、消费dubbo服务（user-consumer）
## 1. 添加依赖
```xml
<dependency>
	<groupId>com.alibaba.spring.boot</groupId>
	<artifactId>dubbo-spring-boot-starter</artifactId>
	<version>RELEASE</version>
	<scope>compile</scope>
</dependency>
<dependency>
	<groupId>com.101tec</groupId>
	<artifactId>zkclient</artifactId>
	<version>0.2</version>
</dependency>
```
## 2. dubbo配置
（1）在src/main/resources下新建文件`application.yml`，配置dubbo：
```yml
spring:
  application:
    name: user-consumer
  dubbo:
    registry:
	  address: zookeeper://ssh.qianxunclub.com:2181
	  
server:
  port: 9000
```
(2) 在`spirng boot`启动类中，添加@EnableDubboConfiguration注解启用dubbo:
```java
package cn.lilyssh.user.consumer;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubboConfiguration
public class UserConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserConsumerApplication.class, args);
	}
}
```
（3）创建Controller，给前端提供Rest接口：
```java
package cn.lilyssh.user.consumer.controller;

import cn.lilyssh.user.consumer.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/user")
public class UserController {

    private UserService userService;

     /**
     * 获取所有用户
     */
    @GetMapping
    public Object userList(){
        return userService.userList();
    }
}
```
（4）创建Service，调用api接口。需用@Reference注解注入api接口服务。
```java
package cn.lilyssh.user.consumer.service;

import cn.lilyssh.user.api.model.response.UserQueryResp;
import cn.lilyssh.user.api.service.UserServiceApi;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class UserService{

    @Reference
    private UserServiceApi userServiceApi;

    public List<UserQueryResp> userList(){
        return userServiceApi.list();
    }
}
```
# 四、用postman测试，调用Rest接口
## 1. 启动user-provider和user-consumer工程
## 2. 在postman中输入测试地址  
http://localhost:9000/user 会看到结果：
```json
[{"id":1,"uuid":"3","userName":"Tom","password":null,"realName":null,"sex":0,"age":0,"phone":0,"email":null,"status":0,"lastLoginIp":null,"lastLoginTime":null,"idType":0,"idNumber":null,"address":null},{"id":2,"uuid":"4","userName":"Mike","password":null,"realName":null,"sex":0,"age":0,"phone":0,"email":null,"status":0,"lastLoginIp":null,"lastLoginTime":null,"idType":0,"idNumber":null,"address":null}]
```

大功告成！