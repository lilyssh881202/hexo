---
title: 高并发架构实战(六) 提高查询性能  
date: 2018-01-06
tags: [架构,性能,高并发架构实战]
categories: 
  - 架构
---

转载请标注原文地址：https://lilyssh.cn/architecture/6-tuning-performance/  
[项目源码地址](https://gitee.com/lilyssh/high-concurrency)
<!-- more -->
在不采取任何措施下，从160万数据中查询某用户的订单，耗时14895毫秒：
![](https://resource.lilyssh.cn/pic/query_no_index.png)
# 一、初步优化，加索引
```sql
ALTER  TABLE  demo.`order`  ADD  INDEX index_order_user_id (  `user_id`  );
```
加索引后 耗时56毫秒：
![](https://resource.lilyssh.cn/pic/query_index.png)

在order是三千万条数据，且使用模糊查询时，直接报`TimeoutException`了：
```
com.alibaba.dubbo.rpc.RpcException: Failed to invoke the method list in the service cn.lilyssh.order.api.service.OrderServiceApi. Tried 3 times of the providers [192.168.0.105:3333] (1/1) from the registry ssh.qianxunclub.com:2181 on the consumer 192.168.0.105 using the dubbo version 2.6.0. Last error is: Invoke remote method timeout. method: list, provider: dubbo://192.168.0.105:3333/cn.lilyssh.order.api.service.OrderServiceApi?anyhost=true&application=order-consumer&check=false&default.timeout=5000&dubbo=2.6.0&generic=false&interface=cn.lilyssh.order.api.service.OrderServiceApi&methods=saveBatch,save,list&pid=1468&register.ip=192.168.0.105&remote.timestamp=1537974222744&side=consumer&timestamp=1537974194871.
	...
Caused by: com.alibaba.dubbo.remoting.TimeoutException: Waiting server-side response timeout. start time: 2018-09-26 23:05:43.949, end time: 2018-09-26 23:05:48.952, client elapsed: 1 ms, server elapsed: 5002 ms, timeout: 5000 ms, request: Request [id=6, version=2.0.0, twoway=true, event=false, broken=false, data=RpcInvocation [methodName=list, parameterTypes=[class cn.lilyssh.order.api.model.request.OrderQueryReq], arguments=[OrderQueryReq(id=null, userId=null, userUuid=null, payment=null, payType=null, postFee=null, status=null, createTime=null, updateTime=null, payTime=null, cosignTime=null, endTime=null, closeTime=null, shippingName=拼, shippingCode=null)], attachments={path=cn.lilyssh.order.api.service.OrderServiceApi, interface=cn.lilyssh.order.api.service.OrderServiceApi, version=0.0.0, timeout=5000}]], channel: /192.168.0.105:51181 -> /192.168.0.105:3333
	at com.alibaba.dubbo.rpc.cluster.support.FailoverClusterInvoker.doInvoke(FailoverClusterInvoker.java:77) ~[dubbo-2.6.0.jar:2.6.0]
	... 70 common frames omitted
```
直接查数据库，也查不出：
![](https://resource.lilyssh.cn/pic/order_can%27t_query.png)
# 二、使用ES
大功告成！