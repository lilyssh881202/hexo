---
title: 高并发架构实战(八) Spring Boot 集成 elasticsearch  
date: 2018-01-08
tags: [架构,高并发架构实战,Spring Boot,Spring Cloud Config]
categories: 
  - 架构
---
转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/82948369  

Spring Boot 2.0.4 集成 [elasticsearch](https://www.elastic.co/cn/products/elasticsearch) 6.4.1。
[项目源码地址](https://gitee.com/lilyssh/high-concurrency)
<!-- more -->
# 一、简介
ElasticSearch是一个基于Lucene的分布式的全文搜索引擎，基于RESTful web接口。
# 二、使用方法
## （1）添加elasticsearch依赖
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-elasticsearch</artifactId>
</dependency>
```
## （2）在`application.yml`中添加elasticsearch配置
```yml
# elasticsearch
spring:
  data:
    elasticsearch:
      cluster-name: my-application  #集群名，默认elasticsearch
      cluster-nodes: 192.168.0.111:9200,192.168.0.112:9200 #节点地址，多个节点用逗号隔开
```
## （3）实体类添加@Document注解
 `indexName`：索引名称，可以理解为数据库名，必须为小写，不然会报错：`org.elasticsearch.indices.InvalidIndexNameException`
`type`：类型，可以理解为表名。
```java
package cn.lilyssh.order.provider.dao.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Document(indexName = "demo",type = "order", shards = 1,replicas = 0, refreshInterval = "-1")
public class OrderESEntity implements Serializable {

    private static final long serialVersionUID = 551589397625941750L;

    private Integer id;
    private Integer userId;
    private String userUuid;
    private BigDecimal payment;
    private Integer payType;
    private BigDecimal postFee;
    private Integer status;
    private Date createTime;
    private Date updateTime;
    private Date payTime;
    private Date cosignTime;
    private Date endTime;
    private Date closeTime;
    private String shippingName;
    private String shippingCode;
}
```
## （4）repository继承ElasticsearchRepository
```java
package cn.lilyssh.order.provider.dao.esrepository ;

import cn.lilyssh.order.provider.dao.entity.OrderESEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface OrderESRepository extends ElasticsearchRepository<OrderESEntity,Integer> {

}
```
## （5）服务实现类
```java
package cn.lilyssh.order.provider.service;

import cn.lilyssh.order.api.model.request.OrderInsertReq;
import cn.lilyssh.order.api.model.request.OrderQueryReq;
import cn.lilyssh.order.api.model.response.OrderQueryResp;
import cn.lilyssh.order.api.service.OrderServiceApi;
import cn.lilyssh.order.provider.dao.entity.OrderESEntity;
import cn.lilyssh.order.provider.dao.entity.OrderEntity;
import cn.lilyssh.order.provider.dao.esrepository.OrderESRepository;
import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@Component
@AllArgsConstructor
public class OrderService implements OrderServiceApi {

    private OrderESRepository orderESRepository;

    /**
     * 根据条件从ES中查询订单列表
     * @param orderQueryReq
     * @return
     */
    @Override
    public List<OrderQueryResp> listByEs(OrderQueryReq orderQueryReq) {
        SearchQuery searchQuery = new NativeSearchQuery(
                new BoolQueryBuilder()
                        .must(QueryBuilders.matchQuery("id",orderQueryReq.getId()))
        );
        Page<OrderESEntity> orderESEntityPage = orderESRepository.search(searchQuery);
        List<OrderESEntity> orderESEntityList = orderESEntityPage.getContent();
        List<OrderQueryResp> orderQueryRespList=new ArrayList<>();
        for (OrderESEntity orderESEntity: orderESEntityList) {
            OrderQueryResp orderQueryResp=new OrderQueryResp();
            BeanUtils.copyProperties(orderESEntity,orderQueryResp);
            orderQueryRespList.add(orderQueryResp);
        }
        return orderQueryRespList;
    }

    /**
     * 保存到ES
     * @param orderInsertReq
     * @return
     */
    @Override
    public boolean saveByEs(OrderInsertReq orderInsertReq){
        OrderESEntity orderESEntity = new OrderESEntity();
        OrderEntity orderEntity=new OrderEntity();

        //TODO 直接写入数据库太慢，引起dubbo超时，导致调用多次，此处需要后期改造成kafka异步写入。
        BeanUtils.copyProperties(orderInsertReq,orderEntity);
        orderRepository.save(orderEntity);

        BeanUtils.copyProperties(orderEntity,orderESEntity);
        orderESRepository.save(orderESEntity);

        return true;
    }
}

```
# 三、启动ES服务
- （1）后台启动Elasticsearch
在`elasticsearch-6.3.2/bin`下执行：
```
./elasticsearch -d 
```
- （2）后台启动elasticsearch-head
elasticsearch-head是一个界面化的集群操作和管理工具，可以对集群进行傻瓜式操作。
在elasticsearch-head根目录下执行：
```
nohup npm run start &
```
- （3）访问es `http://192.168.0.111:9100/`
![]() 
# 四、测试
![]() 
大功告成！