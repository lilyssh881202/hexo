---
title: 高并发架构实战(二) Spring Boot 集成 druid  
date: 2018-01-02
tags: [架构,Spring Boot,Druid,高并发架构实战]
categories: 
  - 架构
---
转载请标注原文地址：转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/82753582  

Spring Boot 2.0.4 集成 druid 1.1.10 。[项目源码地址](https://gitee.com/lilyssh/high-concurrency)  
<!-- more -->
# 1. 初始化工程
工程 user-provider的结构为：
```
$ tree -I target
.
├── pom.xml
└── src
    └── main
        ├── java
        │   └── cn
        │       └── lilyssh
        │           └── user
        │               └── provider
        │                   └── UserProviderApplication.java
        └── resources
            └── application.yml
```
# 2. 添加依赖
打开 `pom.xml` 引入`druid-spring-boot-starter`、`mysql-connector-java`、`spring-boot-starter-jdbc`、`lombok` 依赖：(引入lombok是为了方便实体类中get、set方法的自动生成，并不是连接池需要)
```xml
	<dependency>
		<groupId>com.alibaba</groupId>
		<artifactId>druid-spring-boot-starter</artifactId>
		<version>1.1.10</version>
	</dependency>
	<dependency>
		<groupId>mysql</groupId>
		<artifactId>mysql-connector-java</artifactId>
		<version>8.0.12</version>
	</dependency>
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-jdbc</artifactId>
		<version>2.0.4.RELEASE</version>
	</dependency>
	<dependency>
		<groupId>org.projectlombok</groupId>
		<artifactId>lombok</artifactId>
		<version>1.16.20</version>
		<scope>provided</scope>
	</dependency>
```
# 3. 配置
 (1) 在 `application.yml`配置文件中添加数据库的相关配置：
```yml
spring:
  datasource:
    druid:
      url: jdbc:mysql://db.qianxunclub.com:3306/demo
      username: zhangbin
      password: ghdxZSuz+strVRCKInFX8Vp1vjMksZgb5tELqcc7Gkv7c2pQEFSFGDCy8qfbCZdRdG4VVVwS7rIKCFGWZh0OgQ==
      filters: config
      connection-properties: "config.decrypt=true;config.decrypt.key=MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALCr8UOmkUhwi+xhbpYaSPOMPxw/Rh3KCzIS9VPTo6fleRLXAP9zCC+9s9f88l/GuN4PI4yDPqg4qT6iQpsREA0CAwEAAQ=="

```
(2) 上一步的数据库密码 加密步骤：  
(2.1) 执行命令加密数据库密码
```
$ cd ~/.m2/repository/com/alibaba/druid/1.1.10

$ java -cp druid-1.1.10.jar com.alibaba.druid.filter.config.ConfigTools you_password
```
会看到
```
privateKey: xxx
publicKey: ***
password: &&&
```
输入你的数据库密码，输出的是加密后的结果。
(2.2) 配置数据源，提示Druid数据源需要对数据库密码进行解密。
```
datasource:
    druid:
      url: jdbc:mysql://db.qianxunclub.com:3306/demo
      username: zhangbin
      password: ${password}
      filters: config
      connection-properties: config.decrypt=true;config.decrypt.key=${publickey}
```
# 4. 测试
mvn clean package，报错：
```
[ERROR] Failed to execute goal org.apache.maven.plugins:maven-compiler-plugin:3.1:compile (default-compile) on project user-api: Fatal error compiling: java.lang.ExceptionInInitializerError: com.sun.tools.javac.code.TypeTags -> [Help 1]
[ERROR] 
[ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
[ERROR] Re-run Maven using the -X switch to enable full debug logging.
[ERROR] 
[ERROR] For more information about the errors and possible solutions, please read the following articles:
[ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException

```
解决办法：
这是java10下编译lombok 1.16.20版本的问题，通过升级到1.16.22即可搞定。
```
<dependency>
	<groupId>org.projectlombok</groupId>
	<artifactId>lombok</artifactId>
	<version>1.16.22</version>
	<scope>provided</scope>
</dependency>
```
用`jmeter`压测，会出现错误：线程池耗尽。具体解决办法参考文章：[threadpool is exhausted](https://lilyssh.cn/exception/1-threadpool-is-exhausted/)
大功告成！