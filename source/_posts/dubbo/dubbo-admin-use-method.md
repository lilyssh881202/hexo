---
title: 部署和使用dubbo-admin做服务治理
date: 2018-05-21
tags: [Dubbo]
categories: 
  - Java
  - Dubbo
---

转载请标注原文地址：https://lilyssh.cn/dubbo/dubbo-admin-use-method/   
# 1、下载dubbo-admin源码
```
git clone https://github.com/apache/incubator-dubbo-ops.git
```
会下载下来三个项目：
- dubbo-admin：dubbo控制台(本章重点，敲黑板)
- dubbo-monitor-simple：监控
- dubbo-registry-simple：等我知道这是干啥的了，再来补充。  
<!-- more -->

# 2、修改配置文件
修改`webapp/WEB-INF/dubbo.properties`  
![](https://lilyssh.github.io/pic/dubbo.properties.png)  
```
# zookeeper注册中心地址
dubbo.registry.address=zookeeper://qianxunclub.com:2181
# 默认root用户
dubbo.admin.root.password=root
# 默认guest用户
dubbo.admin.guest.password=guest
```
如果需要，可修改或添加用户名密码
```
dubbo.admin.用户名.password=密码
```
# 3、启动
用tomcat启动本项目，访问`http://localhost:8080/`，并使用上一步`dubbo.properties`配置好的用户名密码登陆。  
# 4、控制台使用
控制台所展示的数据，是从zookeeper里读取到的数据。  

![](https://lilyssh.github.io/pic/dubbo-admin-services.png)  
菜单功能描述：
- services:已注册到zookeeper的dubbo服务接口列表
- applications：已注册到zookeeper的dubbo服务列表
- addresses：所有服务提供者和消费者的ip和端口号列表，点进去会显示，该ip端口下所有的服务信息，包括提供的，消费的，路由，权重等。

其他菜单，可自行研究。并且还提供了很多服务治理的操作，如:禁用/启用服务，设置权重，添加路由等等。
