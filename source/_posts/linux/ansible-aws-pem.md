---
title: ansible 使用亚马逊云服务(AWS)密钥文件 xx.pem同时操纵多台机子
date: 2018-08-10
tags: [Linux,Ansible]
categories: 
  - Linux
---
转载请标注原文地址：https://blog.csdn.net/lilyssh/article/details/82911903  
## 一、安装[ansible](https://www.ansible.com)

1. 安装[pip](https://pip.pypa.io/en/stable/installing/)包管理工具,先下载安装脚本`get-pip.py`。  

```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
```
<!-- more -->
2. 执行安装脚本。  

```
python get-pip.py
```
验证下是否安装成功。
```
pip --version
```
3. 安装 ansible  

```
sudo pip install ansible
```
验证下是否安装成功。
```
ansible --version
```
## 二、 配置[ansible](https://www.ansible.com)。  

在`/etc`下新建文件夹`ansible`，在`/etc/ansible`下创建`hosts`文件，内容如下：
```
[aws]
13.59.244.124	ansible_ssh_private_key_file=~/cypherium_internal.pem
18.216.16.200   ansible_ssh_private_key_file=~/cypherium_internal.pem
```
aws是自定义的服务器组名。
## 三、使用[ansible](https://www.ansible.com)。

`ping`一下分组中的机子，用`ping`模块， `-u 用户名`。
```
ansible aws -m ping -u ubuntu
```
![](https://resource.lilyssh.cn/pic/ansible_ping.png)
`ls` 一下分组中的机子，用`shell`模块：
```
ansible aws -m shell -a "ls" -u ubuntu
```
![](https://resource.lilyssh.cn/pic/ansible_ls.png)